// importa el dal de las cosas
const dalCosas = require("../dal/cosa.dal");
const dalTipoCosa = require("../dal/table.dal")("TipoCosa");

// crea el controller
const controller = {};

controller.cosasView = async function (req, res) {

    // obtiene las cosas de la base de datos
    const cosas = await dalCosas.obtenerTodo();

    // muestra la vista
    res.render("cosas/cosas", {
        cosas: cosas
    });

}

controller.nuevaCosaView = async function (req, res) {

    // obtiene el listado de tipos de cosas para seleecionar
    const tipos = await dalTipoCosa.obtenerTodo();

    // manda la vista
    res.render("cosas/nuevaCosa", {
        tipos: tipos
    });
}

controller.editarCosaView = async function (req, res) {

    // obtiene el id de la url
    const id = req.params.id;

    // obtiene la cosa de la base de datos
    const cosa = await dalCosas.obtenerUno(id);

    // obtiene el listado de tipos de cosas para seleecionar
    const tipos = await dalTipoCosa.obtenerTodo();

    // muestra la vista, con la cosa seleccionada
    res.render("cosas/editarCosa", {
        tipos: tipos,
        cosa: cosa
    });
}

controller.eliminarCosaView = async function (req, res) {

    // obtiene el id de la url
    const id = req.params.id;

    // obtiene la cosa de la coleccion
    const cosa = await dalCosas.obtenerUno(id);

    // muestra la vista, con la cosa seleccionada
    res.render("cosas/eliminarCosa", {
        cosa: cosa
    });
}

controller.nuevaCosa = async function (req, res) {

    // inserta la cosa nueva
    await dalCosas.insertar({
        nombre: req.body.nombre,
        idTipoCosa: req.body.idTipoCosa
    });

    // redirecciona al listado de cosas
    res.redirect("/cosas");
}

controller.editarCosa = async function (req, res) {

    // edita la cosa
    await dalCosas.actualizar(req.body);

    // redirecciona
    res.redirect("/cosas");
}

controller.eliminarCosa = async function (req, res) {

    // elimina la cosa
    await dalCosas.eliminar(req.body.id);

    // redirecciona
    res.redirect("/cosas");
}

// exporta el controller
module.exports = controller;
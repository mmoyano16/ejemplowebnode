const db = require("./base.dal");
const dal = {};

dal.obtenerTodo = function () {
    // return db.select("Cosa");
    const query = `
        select 
            c.id as id,
            c.nombre as nombre,
            c.idTipoCosa as idTipoCosa,
            t.nombre as tipo
        from
            Cosa c
                inner join TipoCosa t on t.id = c.idTipoCosa
    `;
    return db.query(query);
}

dal.obtenerUno = function (id) {
    return db.findOne("Cosa", { id: id });
}

dal.insertar = function (cosa) {
    // devuelve la PK de la cosa que se crea
    return db.insert("Cosa", cosa);
}

dal.actualizar = function (cosa) {
    return db.update("Cosa", { id: cosa.id }, cosa);
}

dal.eliminar = function (id) {
    return db.delete("Cosa", { id: id });
}

module.exports = dal;

const express = require("express");
const exphbs = require('express-handlebars');
const path = require("path");
const app = express();

// funciones principales de la aplicacion
// ----------------------------------------

function configurarViewEngine() {
    /*
        El View Engine o motor de vistas, es una libreria
        que permite exribir nuestro HTML de manera que podemos
        reemplazar valores que "etiquetamos" con valores 
        que mandamos desde el servidor, por ejemplo:

        desde la base de datos, obtenemos una persona:
        const persona = {
            nombre: "chango",
            edad: 20
        }

        si queremos mostrar ese chango en la vista, debemos hacer

        <h1>Informacion del chango</h1>
        <span>El nombre es: {{ chango.nombre }}</span>
        <span>La edad es: {{ chango.edad }}</span>
   
        como funciona??

        cuando hacemos una peticion de esa pagina, primero el servidor
        procesa todas esas etiquetas con cosas que se ejecutan en el servidor,
        y recien ahi devuelve un html completo con las cosas cambiadas
   
    */
    const helpers = require('handlebars-helpers')();
    app.engine('.hbs', exphbs({ extname: '.hbs', helpers: helpers }));
    app.set('view engine', '.hbs');
    app.use(express.json())
    app.use(express.urlencoded({ extended: true }))


}
function configurarRutas() {

    // importa controladores
    const homeController = require("./controllers/home.controller");
    const cosasController = require("./controllers/cosas.controller");
    const tiposController = require("./controllers/tipos.controller");

    // configura las rutas
    app.get("/", homeController.index);
    app.get("/cosas", cosasController.cosasView);
    app.get("/cosas/nueva", cosasController.nuevaCosaView);
    app.get("/cosas/editar/:id", cosasController.editarCosaView);
    app.get("/cosas/eliminar/:id", cosasController.eliminarCosaView);
    app.post("/cosas", cosasController.nuevaCosa);
    app.post("/cosas/editar", cosasController.editarCosa);
    app.post("/cosas/eliminar", cosasController.eliminarCosa);

    app.get("/tipos", tiposController.indexView);
    app.post("/tipos/registrar", tiposController.registrar);
    app.post("/tipos/editar/:id", tiposController.editarView);
    app.post("/tipos/eliminar/:id", tiposController.eliminar);
}
function configurarEstilos() {
    app.use(express.static(path.resolve(__dirname,"node_modules/bootstrap/dist")));
}

function iniciarServidor() {

    // llama a todas las funciones de configuracion
    configurarViewEngine();
    configurarRutas();
    configurarEstilos();

    // ejecuta el servidor
    app.listen(3000, () => {
        console.log("servidor escuchando en el puerto 3000");
    })
}

// inicio de la aplicacion
// ----------------------------------------
iniciarServidor();

